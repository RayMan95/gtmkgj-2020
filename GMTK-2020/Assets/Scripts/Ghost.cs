﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ghost : MonoBehaviour
{
    public float speed = 3f;

    private Player player;

    // Start is called before the first frame update
    void Start()
    {
        player = GameController.instance.player;
    }

    // Update is called once per frame
    void Update()
    {
        if (player == null)
        {
            Die();
            return;
        }
        Vector3 target = player.gameObject.transform.position;

        transform.position = Vector2.MoveTowards(transform.position, target, speed * Time.deltaTime);

    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        Player p = collision.gameObject.GetComponent<Player>();
        if (p != null)
        {
            p.Hit();
            Die();
        }

    }

    void Die()
    {
        // Animations and stuff

        Destroy(gameObject);
    }
}
