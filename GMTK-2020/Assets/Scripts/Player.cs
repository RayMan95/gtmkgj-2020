﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public int maxHp = 4;
    private int hp = 4;

    public float maxBootsTime = 4f;
    private float bootsTime = 0f;
    

    public float speed = 1000f;
    public float upForce = 200f;
    public float gravityScale = 2.0f;

    private bool isTopDown = false;

    private bool hasBoots = false;

    private Rigidbody2D rb2D;

    private AudioSource audioSource;
    public AudioClip[] clips; // [jump1, jump2, hit1, hit2, hit3]

    void Awake()
    {
        rb2D = GetComponent<Rigidbody2D>();
        rb2D.gravityScale = gravityScale;
        audioSource = GetComponent<AudioSource>();
        hp = maxHp;
    }

    void Update()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            rb2D.AddForce(new Vector2(0, upForce), ForceMode2D.Impulse);
            audioSource.PlayOneShot(clips[1]);
        }

        if (hasBoots)
        {
            bootsTime -= Time.deltaTime;

            if (bootsTime <= 0)
            {
                hasBoots = false;
                GameController.instance.HasBoots(false);
            }
        }

    }


    void FixedUpdate()
    {

        Vector2 currentPosition = transform.position;

        float xChange = Input.GetAxis("Horizontal") * speed * Time.deltaTime;
        float yChange = Input.GetAxis("Vertical") * speed * Time.deltaTime;

        //if (isTopDown)
        //else if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.Space))
        

        //if (!isRunning && (xChange > 0 || yChange > 0))
        //{
        //    isRunning = true;
        //}
        //else if (isRunning && (xChange == 0 && yChange == 0))
        //{
        //    isRunning = false;
        //}

        //animator.SetBool("isRunning", isRunning);
        
        if (isTopDown) rb2D.velocity = new Vector2(xChange, yChange); 
        else rb2D.velocity = new Vector2(xChange, rb2D.velocity.y);
    }

    internal void Flip()
    {
        rb2D.velocity = Vector2.zero;
        rb2D.angularVelocity = 0f;        

        if (isTopDown)
        {
            rb2D.gravityScale = gravityScale;
            isTopDown = false;
        }
        else
        {
            rb2D.gravityScale = 0;
            isTopDown = true;
        }
        audioSource.PlayOneShot(clips[0]);
    }

    public void Reset()
    {
        
    }

    public void Hit()
    {
        if (hasBoots) return;

        audioSource.PlayOneShot(clips[UnityEngine.Random.Range(2,4)]);

        hp -= 1;
        GameController.instance.UpdateHealth(false);
        Debug.Log("Player HP: " + hp);

        if (hp <= 0)
        {
            Die();
        }
    }

    

    public void Fall()
    {
        if (isTopDown) Die();
    }

    internal void PickUp(string pickup)
    {
        Debug.Log("Picked up: " + pickup);
        switch (pickup)
        {
            case "Boots":
                bootsTime = maxBootsTime;
                hasBoots = true;
                GameController.instance.HasBoots(true) ;
                SetSprite("Boots");
                break;

            case "Health":
                if (hp >= maxHp) break;
                else
                {
                    hp += 1;
                    GameController.instance.UpdateHealth(true);
                    break;
                }

            default:
                Debug.Log("Unknown Pickup");
                break;
        }
    }

    private void SetSprite(string spriteName)
    {
        
    }

    void Die()
    {
        // Anims and stuff
        GameController.instance.PlayerDied();
        Destroy(gameObject);
    }

}
