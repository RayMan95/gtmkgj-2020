﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

public class GameController : MonoBehaviour
{
    [HideInInspector]
    public static GameController instance;

    public GameObject playerPrefab;
    [HideInInspector]
    public Player player;
    public GameObject textDead;

    public float timeBetweenFlips = 3f;
    private float timeUntilFlip = 0f;
    public TextMeshProUGUI textTimer;

    public float levelMaxTime = 30f;
    private float levelTimeLeft = 0f;
    public TextMeshProUGUI textTimeLeft;

    public float timeBetweenPickupSpawns = 4f;
    public float timeUntilPickupSpawn = 0f;
    public GameObject healthPrefab;
    public GameObject bootPrefab;
    private int currentAvailablePickups;
    private int maxAvailablePickups = 4;

    private float timeBetweenEnemySpawns = 6f;
    private int currentActiveSpawners = 1;

    private GameObject[] allEnemySpawners = new GameObject[4];
    public GameObject enemySpawnerPrefab;

    public Transform[] pickupSpawnPoints;
    public Transform[] enemySpawnPoints;

    private int lastHeartIndex;
    public GameObject[] hearts;
    public GameObject[] emptyHearts;

    public GameObject bootsImage;

    void Awake()
    {
        if (instance == null)
            instance = this;
        else if (instance != this)
            Destroy(gameObject);

        allEnemySpawners[0] = Instantiate(enemySpawnerPrefab, enemySpawnPoints[0].position, Quaternion.identity);
        allEnemySpawners[1] = Instantiate(enemySpawnerPrefab, enemySpawnPoints[1].position, Quaternion.identity);
        allEnemySpawners[2] = Instantiate(enemySpawnerPrefab, enemySpawnPoints[2].position, Quaternion.identity);
        allEnemySpawners[3] = Instantiate(enemySpawnerPrefab, enemySpawnPoints[3].position, Quaternion.identity);
    }

    private void Start()
    {
        timeUntilFlip = timeBetweenFlips;

        textTimer.text = timeUntilFlip.ToString();
        levelTimeLeft = levelMaxTime;

        SetLevel();

    }

    private void SetLevel()
    {
        player = Instantiate(playerPrefab, new Vector3(0f, 0.1f, 0f), Quaternion.identity).GetComponent<Player>();
        lastHeartIndex = player.maxHp - 1;

        currentAvailablePickups = 0;

        var random = new System.Random();

        var list = new List<int> { 0, 1, 2, 3};

        int i = 0;
        {
            while (i < currentActiveSpawners)
            {
                if (i > 3) break;
                
                int index = random.Next(list.Count);
                EnemySpawner es = allEnemySpawners[index].GetComponent<EnemySpawner>(); 
                es.on = true;
                es.timeBetweenSpawns = timeBetweenEnemySpawns;
                i++;
            }
        }

        Debug.Log("currentActiveSpawners: " + currentActiveSpawners);
        Debug.Log("timeBetweenEnemySpawns: " + timeBetweenEnemySpawns);
    }
    private void NextLevel()
    {
        ResetLevel();
        
        currentActiveSpawners++;
        if (currentActiveSpawners > 3) currentActiveSpawners = 4;

        timeBetweenEnemySpawns -= 1f;
        if (timeBetweenEnemySpawns <= 0) timeBetweenEnemySpawns = 0.5f;

        SetLevel();
    }


    private void ResetLevel()
    {
        Destroy(player.gameObject);

        levelTimeLeft = levelMaxTime;
        timeUntilPickupSpawn = 0f;

        foreach (GameObject es in allEnemySpawners)
        {
            es.GetComponent<EnemySpawner>().Reset();
        }
    }

    private void Update()
    {
        
        if (player == null)
        {
            if (Input.GetKeyDown(KeyCode.R))
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
            else if (Input.GetKeyDown(KeyCode.Q))
            {
                SceneManager.LoadScene(0);
            }
            return;
        }
        else
        {
            //UpdateHealthBox();
            levelTimeLeft -= Time.deltaTime;
            if (levelTimeLeft <= 0)
            {
                NextLevel();
            }
            textTimeLeft.text = Mathf.Round(levelTimeLeft).ToString();

            timeUntilFlip -= Time.deltaTime;
            //textTimer.text = Mathf.Round(timeUntilFlip).ToString();
            if (timeUntilFlip <= 0)
            {
                player.Flip();
                timeUntilFlip = timeBetweenFlips;
            }

            timeUntilPickupSpawn -= Time.deltaTime;
            //textTimer.text = Mathf.Round(timeUntilFlip).ToString();
            if (timeUntilPickupSpawn <= 0)
            {
                if (currentAvailablePickups == maxAvailablePickups) return;
                SpawnPickup();
                timeUntilPickupSpawn = timeBetweenPickupSpawns;
            }
        }
    }

    private void SpawnPickup()
    {
        var list = new List<int> { 0, 1, 2, 3, 4, 5};

        var random = new System.Random();

        int index = random.Next(list.Count);

        int choice = UnityEngine.Random.Range(0, 2);
        GameObject chosenPrefab =  choice == 0 ? healthPrefab : bootPrefab;

        Pickup pickup = Instantiate(chosenPrefab, pickupSpawnPoints[index].position, Quaternion.identity).GetComponent<Pickup>() ;

    }

    internal void PlayerDied()
    {
        foreach (GameObject enemySpawner in allEnemySpawners)
        {
            Destroy(enemySpawner);
        }

        textDead.SetActive(true);

    }

    internal void HasBoots(bool hasBoots)
    {
        bootsImage.SetActive(hasBoots);
    }

    internal void UpdateHealth(bool addHp)
    {
        hearts[lastHeartIndex].SetActive(addHp);
        emptyHearts[lastHeartIndex].SetActive(!addHp);

        lastHeartIndex += addHp ? 1 : -1;
    }
}
