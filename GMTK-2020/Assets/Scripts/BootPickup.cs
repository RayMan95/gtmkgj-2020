﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BootPickup : MonoBehaviour
{
    private string pickupName = "Boots";

    void OnTriggerEnter2D(Collider2D collision)
    {
        Player player = collision.gameObject.GetComponent<Player>();

        if (player != null) player.PickUp(pickupName);

        Destroy(gameObject);
    }
}
