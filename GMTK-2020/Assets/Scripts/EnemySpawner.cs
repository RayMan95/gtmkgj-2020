﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    public GameObject enemyPrefab;

    public float timeBetweenSpawns = 3f;
    private float timeUntilSpawn = 0f;

    public bool on = false;


    public void Reset()
    {
        on = false;
        timeUntilSpawn = 0f;
    }

    void Update()
    {
        if (on)
        {
            if (timeUntilSpawn > 0)
            {
                timeUntilSpawn -= Time.deltaTime;
            }
            else
            {
                //Debug.Log("Enemy spawned");
                timeUntilSpawn = timeBetweenSpawns;
                Instantiate(enemyPrefab, transform.position, Quaternion.identity);


            }
        }
    }
}
